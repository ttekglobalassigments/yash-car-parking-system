# For Java 17,
FROM openjdk:17-oracle
WORKDIR /opt/app/yash-car-park-system
COPY ./target/*.jar /opt/app/yash-car-park-system/Car-Park.jar
ENTRYPOINT ["java","-jar","Car-Park.jar"]