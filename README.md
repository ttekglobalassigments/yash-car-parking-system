# Car Parking System

This Java-based API-only application, developed with the Spring Boot framework, serves as a tool to furnish users with
details regarding nearby car parks, including information on their availability. The application seamlessly integrates
with a PostgreSQL database, ensuring efficient storage and retrieval of comprehensive car park details and availability
status.

## Installations

* Apache Maven
* Docker
* Docker Compose

## Application Flow and Assumptions

* On application initialisation, the function `loadParkingDataset()` will upload all car parks seed data onto the
  database.

* In order to update car park status, we need to send a kafka request.

* The kafka request dto is in respect to the json object we get in response from the official gov
  site [car park availibity](https://api.data.gov.sg/v1/transport/carpark-availability)

* Used Haversine formula to calculate distance between coordinates.

* Created a custom coordinate converter class to convert SVY21 to latitude and longitude as there was no supported
  library for java 17.

## Running the Application

1. Clone the Repository: Clone this repository to your local machine using Git.

```bash
git clone https://gitlab.com/ttekglobalassigments/yash-car-parking-system.git
or
git clone git@gitlab.com:ttekglobalassigments/yash-car-parking-system.git
```

2. Build the Application: Navigate to the project directory and build the application using Maven.

```bash
cd yash-car-parking-system
mvn clean install
```

3. Run Docker Containers: Run docker compose file to build and run all services.

```bash
docker-compose up --build
```

### Update Car Park availability using a Kafka

4. Send a request to kafka consumer: Navigate to another terminal and run the following command from kafka producer to
   send request to kafka consumer

```bash
cat data.json | docker-compose exec -T kafka kafka-console-producer.sh --broker-list kafka:9092 --topic park-availability-topic
```

5. Get the nearest car parks: The following api will return all the nearest car parks from your location

```bash
localhost:8081/carparks/nearest?latitude=1.37326&longitude=103.897&page=2&perPage=3
```

##### Note: Request parameter latitude and longitude are compulsory fields while page and per page have default values 1 and 50 set respectively.

6. Shut down application: In order to shut down the application to need to run the following command in order to close
   all services

```bash
docker-compose down --volumes
```

## Project Structure

- /config - Consists of config files like kafka consumer.
- /controllers - Consists of all controller class and exception handler
- /models - Includes entity models
- /dao - Includes model repository classes
- /services - Consists of service classes with business logic
- /utils - Includes all utils functions used throughout the application
- /util/coordinateConverter - Consists of all custom classes for coordinate conversion and distance calculation
