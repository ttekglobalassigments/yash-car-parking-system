package com.carpark.services;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.carpark.models.CarParkLot;
import com.carpark.models.KafkaCarParkItemDTO;
import com.carpark.models.KafkaCarParkItemDTO.CarparkData;
import com.carpark.models.KafkaCarParkItemDTO.CarparkInfo;
import com.carpark.models.KafkaCarParkItemDTO.CarparkItem;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.kafka.support.Acknowledgment;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class KafkaConsumerServiceImplTest {

    private KafkaConsumerServiceImpl kafkaConsumerServiceImpl;
    @Mock
    private Acknowledgment acknowledgment;

    @Mock
    private ConsumerRecord consumerRecord;

    @Mock
    CarParkLotService carParkLotService;

    List<CarParkLot> carParks = new ArrayList<>();

    @BeforeEach
    void setUp() {
        kafkaConsumerServiceImpl = new KafkaConsumerServiceImpl(carParkLotService);
        List<KafkaCarParkItemDTO.CarparkInfo> carParkInfos = new ArrayList<>();
        carParkInfos.add(new CarparkInfo("2", "C", "1"));
        List<KafkaCarParkItemDTO.CarparkData> carParkData = new ArrayList<>();
        carParkData.add(new CarparkData(carParkInfos, "ASK", LocalDateTime.now().toString()));
        List<KafkaCarParkItemDTO.CarparkItem> items = new ArrayList<>();
        items.add(new CarparkItem(LocalDateTime.now().toString(), carParkData));
        KafkaCarParkItemDTO kafkaCarParksEventDTO = new KafkaCarParkItemDTO(new ArrayList<>());
        kafkaCarParksEventDTO.setItems(items);
        carParks.add(new CarParkLot("Test", "test", 1.0, 1.0, 1, 5));
    }

    @Test
    void testConsumeCarParkLotUpdatedEvent_When_EventDTOIsValid_Then_CallExpectedMethods() {
        doReturn(carParks).when(carParkLotService).findAllCarParks();
        doNothing().when(carParkLotService).saveAllCarParks(any());
        kafkaConsumerServiceImpl.consumeCarParkLotUpdatedEvent(consumerRecord, acknowledgment);
        verify(carParkLotService, times(0)).saveAllCarParks(any());
    }

}


/*
import com.carpark.dao.ParkingLotRepository;
import com.carpark.models.CarParkLot;
import com.carpark.models.CarparkInfo;
import com.carpark.models.CarparkItem;
import com.carpark.models.KafkaCarParkItemDTO;
import com.carpark.models.KafkaCarParkItemDTO.CarparkData;
import com.carpark.models.KafkaCarParkItemDTO.CarparkInfo;
import com.carpark.models.KafkaCarParkItemDTO.CarparkItem;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.kafka.support.Acknowledgment;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@Slf4j
class KafkaConsumerServiceImplTest {

    @Mock
    private ParkingLotRepository parkingLotRepository;

    @InjectMocks
    private KafkaConsumerServiceImpl kafkaConsumerService;

    @Captor
    private ArgumentCaptor<List<CarParkLot>> carParkLotListCaptor;

    @Test
    void testConsumeCarParkLotUpdatedEvent() throws Exception {
        // Create a sample Kafka event DTO
        KafkaCarParkItemDTO kafkaEventDTO = new KafkaCarParkItemDTO();
        CarparkItem carparkItem = new CarparkItem();
        carparkItem.setCarparkData(Arrays.asList(new CarparkData(new CarparkInfo("10", "5"))));
        kafkaEventDTO.setItems(Arrays.asList(carparkItem));

        // Mock the ConsumerRecord and Acknowledgment
        ConsumerRecord<String, String> consumerRecord = mock(ConsumerRecord.class);
        when(consumerRecord.value()).thenReturn(new ObjectMapper().writeValueAsString(kafkaEventDTO));
        Acknowledgment acknowledgment = mock(Acknowledgment.class);

        // Call the method to be tested
        kafkaConsumerService.consumeCarParkLotUpdatedEvent(consumerRecord, acknowledgment);

        // Verify that the repository method saveAll was called with the expected list of CarParkLots
        verify(parkingLotRepository).saveAll(carParkLotListCaptor.capture());

        // Retrieve the captured argument
        List<CarParkLot> capturedCarParkLots = carParkLotListCaptor.getValue();

        // Assertions or verifications based on the expected behavior
        assertEquals(1, capturedCarParkLots.size());
        CarParkLot capturedCarParkLot = capturedCarParkLots.get(0);
        assertEquals(10, capturedCarParkLot.getTotalLots());
        assertEquals(5, capturedCarParkLot.getAvailableLots());

        // Verify that the acknowledgment was acknowledged
        verify(acknowledgment).acknowledge();
    }
}

*/

