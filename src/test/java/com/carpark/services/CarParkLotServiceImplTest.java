package com.carpark.services;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import com.carpark.dao.CarParkRepository;
import com.carpark.models.CarParkLot;
import com.carpark.models.CarParkResponse;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {CarParkLotServiceImpl.class})
@ExtendWith(SpringExtension.class)
class CarParkLotServiceImplTest {

    @MockBean
    private CarParkRepository carParkRepository;

    @Autowired
    private CarParkLotServiceImpl carParkLotService;

    @BeforeEach
    void setUp() {
        //        carParkLotService = new CarParkLotServiceImpl(carParkRepository);
        //        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getNearestParkingLots_Success() {
        // Mock data
        double latitude = 10.0;
        double longitude = 20.0;
        int page = 1;
        int perPage = 5;

        List<CarParkLot> mockCarParkLots = Arrays.asList(
                new CarParkLot("1", "Lot 1", 10.1, 20.1, 20, 10),
                new CarParkLot("2", "Lot 2", 10.2, 20.2, 15, 5),
                new CarParkLot("3", "Lot 3", 10.3, 20.3, 10, 5),
                new CarParkLot("4", "Lot 4", 10.4, 20.4, 15, 5),
                new CarParkLot("5", "Lot 5", 10.5, 20.5, 15, 5));

        when(carParkRepository.fetchAvailableParkingLots()).thenReturn(mockCarParkLots);
        //        doReturn(mockCarParkLots).when(carParkRepository).fetchAvailableParkingLots();
        // Test the method
        List<CarParkResponse> result = carParkLotService.getNearestParkingLots(latitude, longitude,
                page, perPage);

        // Verify the result
        assertEquals(perPage, result.size());
        // Add more assertions based on your specific requirements
    }

    @Test
    void getNearestParkingLots_EmptyList_ReturnsEmptyList() {
        // Mock data
        double latitude = 10.0;
        double longitude = 20.0;
        int page = 1;
        int perPage = 5;

        when(carParkRepository.fetchAvailableParkingLots()).thenReturn(Collections.emptyList());

        List<CarParkResponse> result = carParkLotService.getNearestParkingLots(latitude, longitude,
                page, perPage);

        // Verify the result
        assertEquals(0, result.size());
    }

    @Test
    void getNearestParkingLots_MoreAvailablePerPage_ReturnsPerPage() {
        // Mock data
        double latitude = 10.0;
        double longitude = 20.0;
        int page = 1;
        int perPage = 2;

        List<CarParkLot> mockCarParkLots = Arrays.asList(
                new CarParkLot("1", "Lot 1", 10.1, 20.1, 20, 10),
                new CarParkLot("2", "Lot 2", 10.2, 20.2, 15, 5),
                new CarParkLot("3", "Lot 3", 10.3, 20.3, 10, 5));

        when(carParkRepository.fetchAvailableParkingLots()).thenReturn(mockCarParkLots);

        List<CarParkResponse> result = carParkLotService.getNearestParkingLots(latitude, longitude,
                page, perPage);

        assertEquals(perPage, result.size());
    }


    @Test
    void validateInput_WithValidInput_DoesNotThrowException() {
        // Arrange
        Double latitude = 10.0;
        Double longitude = 20.0;

        // Act & Assert
        assertDoesNotThrow(() -> carParkLotService.validateInput(latitude, longitude));
    }
}