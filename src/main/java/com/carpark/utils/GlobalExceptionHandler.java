package com.carpark.utils;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * GlobalExceptionHandler is a class that handles exceptions globally for controllers in the application. It provides a
 * centralized way to handle specific exception types and customize the response for those exceptions.
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    /**
     * ExceptionHandler for IllegalArgumentException. Handles IllegalArgumentExceptions thrown within controllers.
     *
     * @param ex The IllegalArgumentException that was thrown.
     * @return ResponseEntity with a BAD_REQUEST status and the error message from the exception.
     */
    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<String> handleIllegalArgumentException(IllegalArgumentException ex) {
        // Return a ResponseEntity with a BAD_REQUEST status and the error message from the exception.
        return ResponseEntity.badRequest().body(ex.getMessage());
    }
}
