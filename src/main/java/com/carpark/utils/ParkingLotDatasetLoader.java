package com.carpark.utils;

import com.carpark.models.CarParkLot;
import com.carpark.services.CarParkLotService;
import com.carpark.utils.coordinateConverter.LatLonCoordinate;
import com.carpark.utils.coordinateConverter.SVY21Coordinate;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * ParkingLotDatasetLoader is a component responsible for loading parking lot data from a CSV file into the database. It
 * uses Spring's ApplicationReadyEvent to trigger the data loading process when the application is ready.
 */
@Component
@Slf4j
@RequiredArgsConstructor
public class ParkingLotDatasetLoader {

    // Autowired ResourceLoader for loading resources, and ParkingLotRepository for accessing the database
    @Autowired
    ResourceLoader resourceLoader;

    private final CarParkLotService carParkLotService;

    /**
     * EventListener method triggered when the application is ready. It loads parking lot data from a CSV file into the
     * database if the database is empty.
     */
    @EventListener(ApplicationReadyEvent.class)
    public void loadParkingDataset() {
        try {
            // Check if the database already contains parking lot data
            if (!carParkLotService.findAllCarParks().isEmpty()) {
                return; // Return if data is already present
            }

            // Load the CSV file from the classpath resources
            Resource resource = resourceLoader.getResource("classpath:/datasets/HDBCarparkInformation.csv");
            ArrayList<CarParkLot> datasets = new ArrayList<>();
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(resource.getInputStream()));
                    CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT.withHeader())) {

                // Iterate through CSV records and convert coordinates
                for (CSVRecord csvRecord : csvParser) {
                    LatLonCoordinate latLon = convertSVY21ToLatLon(Double.parseDouble(csvRecord.get("x_coord")),
                            Double.parseDouble(csvRecord.get("y_coord")));

                    // Create ParkingLot objects and add them to the list
                    CarParkLot carPark = new CarParkLot(csvRecord.get("car_park_no"), csvRecord.get("address"),
                            latLon.getLatitude(), latLon.getLongitude());
                    datasets.add(carPark);
                }

                // Save all parking lot data to the database
                carParkLotService.saveAllCarParks(datasets);
            }
        } catch (Exception e) {
            // Log and rethrow a RuntimeException if an error occurs during data loading
            log.error("Unable to load initial dataset with exception " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    /**
     * Convert SVY21 coordinates to LatLon coordinates.
     *
     * @param xCoord The X-coordinate in SVY21.
     * @param yCoord The Y-coordinate in SVY21.
     * @return A LatLonCoordinate object representing latitude and longitude.
     */
    public LatLonCoordinate convertSVY21ToLatLon(double xCoord, double yCoord) {
        SVY21Coordinate svy21Coordinate = new SVY21Coordinate(xCoord, yCoord);
        return svy21Coordinate.asLatLon();
    }
}
