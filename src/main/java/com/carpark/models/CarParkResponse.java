package com.carpark.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CarParkResponse {
    private String address;
    private double latitude;
    private double longitude;
    private int total_lots;
    private int available_lots;
}
