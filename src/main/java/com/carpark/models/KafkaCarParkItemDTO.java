package com.carpark.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class KafkaCarParkItemDTO {
    private List<CarparkItem> items;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class CarparkItem {

        @JsonProperty("timestamp")
        private String timestamp;

        @JsonProperty("carpark_data")
        private List<CarparkData> carparkData;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class CarparkData {

        @JsonProperty("carpark_info")
        private List<CarparkInfo> carparkInfo;

        @JsonProperty("carpark_number")
        private String carparkNumber;

        @JsonProperty("update_datetime")
        private String updateDatetime;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class CarparkInfo {

        @JsonProperty("total_lots")
        private String totalLots;

        @JsonProperty("lot_type")
        private String lotType = "";

        @JsonProperty("lots_available")
        private String lotsAvailable;

        public CarparkInfo(String totalLots, String lotsAvailable) {
            this.lotsAvailable = lotsAvailable;
            this.totalLots = totalLots;
        }
    }
}