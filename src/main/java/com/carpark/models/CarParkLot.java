package com.carpark.models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class CarParkLot {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private long id;
    private String carParkNo;
    private String address;
    private Double lat;
    private Double lon;
    private int totalLots = 0;

    private int availableLots = 0;

    // Additional field for distance
    private transient double distance;

    public CarParkLot(String carParkNo, String address, Double lat, Double lon) {
        this.carParkNo = carParkNo;
        this.address = address;
        this.lat = lat;
        this.lon = lon;
    }

    public CarParkLot(String carParkNo, String address, Double lat, Double lon, int totalLots, int availableLots) {
        this.carParkNo = carParkNo;
        this.address = address;
        this.lat = lat;
        this.lon = lon;
        this.totalLots = totalLots;
        this.availableLots = availableLots;
    }
}
