package com.carpark.controllers;

import com.carpark.models.CarParkResponse;
import com.carpark.services.CarParkLotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/carparks")
public class CarParkController {

    @Autowired
    CarParkLotService parkingLotService;

    @GetMapping("/nearest")
    public List<CarParkResponse> getNearestParkingLots(@RequestParam(required = false) Double latitude,
            @RequestParam(required = false) Double longitude, @RequestParam(defaultValue = "1") int page,
            @RequestParam(defaultValue = "10") int perPage) {
        return parkingLotService.getNearestParkingLots(latitude, longitude, page, perPage);
    }
}
