package com.carpark.dao;

import com.carpark.models.CarParkLot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CarParkRepository extends JpaRepository<CarParkLot, Long> {
    @Query("SELECT p FROM CarParkLot p WHERE CAST(p.availableLots AS integer) > 0")
    List<CarParkLot> fetchAvailableParkingLots();
}
