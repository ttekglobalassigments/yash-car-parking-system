package com.carpark.services;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.support.Acknowledgment;

public interface KafkaConsumerService {

    /**
     * KafkaListener method that consumes car park lot updated events from a Kafka topic.
     *
     * @param consumerRecord The Kafka ConsumerRecord containing the event data.
     * @param acknowledgment The Acknowledgment object for manual acknowledgment.
     */
    void consumeCarParkLotUpdatedEvent(ConsumerRecord<String, String> consumerRecord, Acknowledgment acknowledgment);
}
