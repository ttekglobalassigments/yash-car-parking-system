package com.carpark.services;

import com.carpark.models.CarParkLot;
import com.carpark.models.CarParkResponse;

import java.util.List;

public interface CarParkLotService {
    /**
     * Get a list of the nearest parking lots based on given latitude, longitude, page, and perPage parameters.
     *
     * @param latitude The latitude of the reference point.
     * @param longitude The longitude of the reference point.
     * @param page The page number for pagination.
     * @param perPage The number of items per page for pagination.
     * @return A list of ParkingLotResponse representing the nearest parking lots.
     */
    List<CarParkResponse> getNearestParkingLots(Double latitude, Double longitude, int page, int perPage);

    /**
     * Saves or updates all car parks.
     *
     * @param carParkLots consists of all the car parks to be inserted or updated.
     */
    void saveAllCarParks(List<CarParkLot> carParkLots);

    /**
     * Returns all car parks from your coordinate.
     * <p>
     * This method returns List of CarParkDto object which consists of all the car park details.
     *
     * @return the result is List of CarParkDto.
     */
    List<CarParkLot> findAllCarParks();

    /**
     * Validate latitude and longitude parameters.
     *
     * @param latitude The latitude to be validated.
     * @param longitude The longitude to be validated.
     */
    void validateInput(Double latitude, Double longitude);
}
