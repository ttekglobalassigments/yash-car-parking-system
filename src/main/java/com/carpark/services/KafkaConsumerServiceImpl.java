package com.carpark.services;

import com.carpark.models.CarParkLot;
import com.carpark.models.KafkaCarParkItemDTO;
import com.carpark.models.KafkaCarParkItemDTO.CarparkData;
import com.carpark.models.KafkaCarParkItemDTO.CarparkInfo;
import com.carpark.models.KafkaCarParkItemDTO.CarparkItem;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
@RequiredArgsConstructor
public class KafkaConsumerServiceImpl implements KafkaConsumerService {
    private final CarParkLotService carParkLotService;

    @Override
    @KafkaListener(topics = {"${kafka.topic}"}, containerFactory = "kafkaListenerContainerFactory",
            groupId = "parking-lot")
    public void consumeCarParkLotUpdatedEvent(ConsumerRecord<String, String> consumerRecord,
            Acknowledgment acknowledgment) {
        try {
            // Read the Kafka event data and convert it to ParkingLotItemDTO
            String kafkaEventDTOString = consumerRecord.value();
            KafkaCarParkItemDTO kafkaEventDTO = new ObjectMapper().readValue(kafkaEventDTOString,
                    KafkaCarParkItemDTO.class);

            // Update the data in the repository based on the Kafka event
            updateDataInRepo(kafkaEventDTO);
        } catch (Exception ex) {
            // Log an error if an exception occurs during processing
            log.error(String.format("Error occurred while updating car park lot: " + ex.getMessage()));
        }
    }

    /**
     * Update the data in the repository based on the information in the ParkingLotItemDTO.
     *
     * @param kafkaCarParkItemDTO The ParkingLotItemDTO containing car park lot information.
     */
    private void updateDataInRepo(KafkaCarParkItemDTO kafkaCarParkItemDTO) {
        // Retrieve all parking lots from the repository
        List<CarParkLot> carParkLots = carParkLotService.findAllCarParks();
        // Build a map of car park numbers to CarparkInfo objects
        Map<String, CarparkInfo> carparkInfoMap = buildCarparkInfoMap(kafkaCarParkItemDTO);

        // Update each parking lot's available and total lots based on the map
        for (CarParkLot carParkLot : carParkLots) {
            CarparkInfo carparkInfo = carparkInfoMap.get(carParkLot.getCarParkNo());
            if (carparkInfo != null) {
                carParkLot.setAvailableLots(Integer.parseInt(carparkInfo.getLotsAvailable()));
                carParkLot.setTotalLots(Integer.parseInt(carparkInfo.getTotalLots()));
            }
        }
        // Save the updated parking lots back to the repository
        carParkLotService.saveAllCarParks(carParkLots);
    }

    /**
     * Build a map of car park numbers to CarparkInfo objects.
     *
     * @param kafkaCarParkItemDTO The ParkingLotItemDTO containing car park lot information.
     * @return A map of car park numbers to CarparkInfo objects.
     */
    private Map<String, CarparkInfo> buildCarparkInfoMap(KafkaCarParkItemDTO kafkaCarParkItemDTO) {
        Map<String, CarparkInfo> carparkInfoMap = new HashMap<>();

        // Iterate through the items and car park data in the ParkingLotItemDTO
        for (CarparkItem carparkItem : kafkaCarParkItemDTO.getItems()) {
            for (CarparkData carparkData : carparkItem.getCarparkData()) {
                // Extract car park number and total availability from the car park data
                String carparkNumber = carparkData.getCarparkNumber();
                CarparkInfo totalAvailability = getTotalAvailabilityOfCarPark(carparkData.getCarparkInfo());
                // Put the car park number and total availability in the map
                carparkInfoMap.put(carparkNumber, totalAvailability);
            }
        }
        return carparkInfoMap;
    }

    /**
     * Calculate the total availability of a car park based on a list of CarparkInfo objects.
     *
     * @param carparkInfo A list of CarparkInfo objects containing availability information.
     * @return A CarparkInfo object representing the total availability.
     */
    private CarparkInfo getTotalAvailabilityOfCarPark(List<CarparkInfo> carparkInfo) {
        int availableLots = 0;
        int totalLots = 0;

        // Iterate through the CarparkInfo objects and accumulate the available and total lots
        for (CarparkInfo info : carparkInfo) {
            availableLots += Integer.parseInt(info.getLotsAvailable());
            totalLots += Integer.parseInt(info.getTotalLots());
        }

        // Create a new CarparkInfo object with the total availability
        return new CarparkInfo(String.valueOf(totalLots), String.valueOf(availableLots));
    }
}
