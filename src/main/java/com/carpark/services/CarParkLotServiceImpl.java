package com.carpark.services;

import com.carpark.dao.CarParkRepository;
import com.carpark.models.CarParkResponse;
import com.carpark.models.CarParkLot;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CarParkLotServiceImpl implements CarParkLotService {

    // Earth radius used for distance calculation
    private static final double EARTH_RADIUS = 6371;

    // Autowired ParkingLotRepository for accessing parking lot data
    private final CarParkRepository carParkRepository;

    @Override
    public List<CarParkResponse> getNearestParkingLots(Double latitude, Double longitude, int page, int perPage) {
        // Validate input parameters
        validateInput(latitude, longitude);

        // Fetch available parking lots, calculate distances, and perform sorting and pagination
        return carParkRepository.fetchAvailableParkingLots().stream().peek(parkingLot -> parkingLot.setDistance(
                        calculateDistance(latitude, longitude, parkingLot.getLat(), parkingLot.getLon()))).sorted(
                        Comparator.comparingDouble(CarParkLot::getDistance)).skip((long) (page - 1) * perPage).limit(perPage)
                .map(this::convertToResponse).collect(Collectors.toList());
    }

    @Override
    public void saveAllCarParks(List<CarParkLot> carParkLots) {
        carParkRepository.saveAll(carParkLots);
    }

    @Override
    public List<CarParkLot> findAllCarParks() {
        return carParkRepository.findAll();
    }

    /**
     * Convert ParkingLot object to ParkingLotResponse object.
     *
     * @param carParkLot The ParkingLot object to be converted.
     * @return A ParkingLotResponse object.
     */
    private CarParkResponse convertToResponse(CarParkLot carParkLot) {
        CarParkResponse response = new CarParkResponse();
        response.setLatitude(carParkLot.getLat());
        response.setLongitude(carParkLot.getLon());
        response.setAddress(carParkLot.getAddress());
        response.setTotal_lots(carParkLot.getTotalLots());
        response.setAvailable_lots(carParkLot.getAvailableLots());
        return response;
    }

    public void validateInput(Double latitude, Double longitude) {
        if (latitude == null || longitude == null) {
            throw new IllegalArgumentException("Latitude and longitude are required.");
        }
    }

    /**
     * Calculate the distance between two points on the Earth's surface using Haversine formula.
     *
     * @param startLat The latitude of the starting point.
     * @param startLong The longitude of the starting point.
     * @param endLat The latitude of the ending point.
     * @param endLong The longitude of the ending point.
     * @return The distance between the two points.
     */
    private double calculateDistance(double startLat, double startLong, double endLat, double endLong) {
        double dLat = Math.toRadians(endLat - startLat);
        double dLong = Math.toRadians(endLong - startLong);

        startLat = Math.toRadians(startLat);
        endLat = Math.toRadians(endLat);

        double a = haversine(dLat) + Math.cos(startLat) * Math.cos(endLat) * haversine(dLong);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return EARTH_RADIUS * c;
    }

    /**
     * Calculate the Haversine of a value.
     *
     * @param val The value for which Haversine is calculated.
     * @return The Haversine of the value.
     */
    private double haversine(double val) {
        return Math.pow(Math.sin(val / 2), 2);
    }
}
